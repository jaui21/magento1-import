<?php
require_once '/home/webuser/public_html/app/Mage.php';
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
if (($file = fopen("/home/webuser/public_html/var/import/product-3bot.csv", "r")) !== FALSE) {
	while (($data = fgetcsv($file, 1000, ",")) !== FALSE) {
    //echo $data[0] . "," . $data[1] . "," . $data[2] . "," . $data[3] . "," . $data[4] . "\n" ;
		$SKU = (string)$data[0];
	    $product = Mage::getModel('catalog/product')->loadByAttribute('sku',$SKU);	
		if($product) :
			try{
				$product
			    ->setName((string)$data[1] ) 												// update Product Name							
			    ->setDescription((string)$data[2])  										// update Product Description
			    ->setShortDescription((string)$data[3]);									// update Product Short Description

			    if ($product->getAttributeText('sync') == "No") {							// check if the value is 0 or "NO"			
			    	if(!empty($data[4])){   												// check if the [COLUMN 4] value is EMPTY
					    $fileimage = Mage::getBaseDir('media') . DS . 'catalog/product' . $data[4]; 
				    	if(file_exists($fileimage)){ 					                  	// check if the image file really exist if not the system will return error and exit the script
					    //echo Mage::getBaseDir('media') . DS . 'catalog/product' . $data[4] . "\n";	
					    $product	
					    ->setMediaGallery (array('images'=>array (), 'values'=>array ())) 	//media gallery initialization
					    ->addImageToMediaGallery(Mage::getBaseDir('media') . DS . 'catalog/product' . $data[4], array('image','thumbnail','small_image'), false, false); //assigning image, thumb and small image to media gallery
					    }
					}
				$product->setSync("1"); 										          	// switch the the value to 1 or "YES"
			    }

			// Set the product color
			if(!empty($data[5])){    											          	// check if the [COLUMN 5] value is EMPTY
			$colorattr = $product->getResource()->getAttribute('color');		          	// load color attribute
			$colorid = $colorattr->getSource()->getOptionId((string)$data[5]);            	// get color option ID using the productAttribute text (color label)
		   	$product->setColor($colorid); 										          	// update Product Color
		   	}   
			 			   
			$product->setUpdatedAt(strtotime('now')); 							          	// update time		
			$product->save();
			}
			catch(Exception $e){
			Mage::log($e->getMessage());
			}
		endif;
	} 																			          	// END WHILE
fclose($file);
} else {
	echo "error!!";
}