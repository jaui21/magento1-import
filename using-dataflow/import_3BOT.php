<?php         

    require_once 'app/Mage.php';

    set_time_limit(0);
    ini_set('memory_limit', '128M');

    $root       = "/home/webuser/dev_promotions/";
    $logFile    = 'magento_import.log';

    umask(0);
    $app = Mage::app('default');

    Mage::log("========================== BEGIN IMPORT ==========================", null, $logFile);
    //echo "========================== BEGIN IMPORT ==========================\n";

    // Login Admin User

    Mage::getSingleton('core/session', array('name' => 'adminhtml'));
    $username = "joey.lana";
    $user = Mage::getModel('admin/user')->loadByUsername($username);

    if (Mage::getSingleton('adminhtml/url')->useSecretKey()) {
      Mage::getSingleton('adminhtml/url')->renewSecretUrls();
    }

    $session = Mage::getSingleton('admin/session');
    $session->setIsFirstVisit(true);
    $session->setUser($user);
    $session->setAcl(Mage::getResourceModel('admin/acl')->loadAcl());
    Mage::dispatchEvent('admin_session_user_login_success',array('user'=>$user));

    if ($session->isLoggedIn()) {

        Mage::log("User '" . $username . "' logged in.", null, $logFile);
        //echo "User '" . $username . "' logged in.\n";

    } else {

        Mage::log("ERROR: Could not login as user '" . $username . "'.", null, $logFile);
        //echo "ERROR: Could not login as user '" . $username . "'.\n";

    }

    // Load DataFlow Profile

    $profile_id = 11;

    $profile = Mage::getModel('dataflow/profile');

    $profile->load($profile_id);

    if (!$profile->getId()) {

        Mage::log("ERROR: Profile with ID #{$profile_id} does not exist.", null, $logFile);
        //echo "ERROR: Profile with ID #{$profile_id} does not exist.\n";
        exit;

    }

    Mage::register('current_convert_profile', $profile);

    $profile->run();

    // Begin Bactch Processing

    // Limit of products per batch (max: 50)
    $batchLimit = 50;

    function convert($size)
    {

        $unit=array('b','kb','mb','gb','tb','pb');

        return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];

    }

    $batchModel = Mage::getSingleton('dataflow/batch');

    if (!$batchModel->getId()) {

        Mage::log(convert(memory_get_usage()) . " - ERROR: Can't get batchModel", null, $logFile);
        //echo convert(memory_get_usage()) . " - ERROR: Can't get batchModel\n";
        exit;

    }

    if (!$batchModel->getAdapter()) {

        Mage::log(convert(memory_get_usage()) . " - ERROR: Can't getAdapter", null, $logFile);
        //echo convert(memory_get_usage()) . " - ERROR: Can't getAdapter\n";
        exit;

    }

    $batchId            = $batchModel->getId();
    $batchImportModel   = $batchModel->getBatchImportModel();
    $importIds          = $batchImportModel->getIdCollection();

    $recordCount        = null;
    $totalproducts      = count($importIds);

    $saved              = 0;
    $batchArrayIds      = array();

    foreach ($importIds as $importId) {

        $recordCount++;

        $batchArrayIds[] = $importId;

        if ($recordCount%$batchLimit == 0 || $recordCount == $totalproducts) {

            $paramsArr  = array('batchid' => $batchId, 'ids' => $batchArrayIds);
            $params     = json_encode($paramsArr);
            $result     = array();

            exec("php -f {$root}/batch_import_processor.php '{$params}'", $result);

            $saved += $result[0];

            Mage::log(convert(memory_get_usage()) . " - processed {$recordCount}/$totalproducts. Saved {$result[0]} products.", null, $logFile);
            //echo convert(memory_get_usage()) . " - processed {$recordCount}/$totalproducts. Saved {$result[0]} products.\n";

            $batchArrayIds = array();

        }

    }


    $batchModel = Mage::getModel('dataflow/batch')->load($batchId);

    try {

        $batchModel->beforeFinish();

    } catch (Mage_Core_Exception $e) {

        Mage::log(convert(memory_get_usage()) . " - ERROR: ". $e->getMessage(), null, $logFile);
        //echo convert(memory_get_usage()) . " - ERROR: ". $e->getMessage() . "\n";

    } catch (Exception $e) {

        Mage::log(convert(memory_get_usage()) . " - ERROR: An error occurred while finishing process. Please refresh the cache" . $e->getMessage(), null, $logFile);
        //echo convert(memory_get_usage()) . " - ERROR: An error occurred while finishing process. Please refresh the cache" . $e->getMessage() . "\n";

    }

    $batchModel->delete();

    // Output Debugging Info
    foreach ($profile->getExceptions() as $e) {

        Mage::log(convert(memory_get_usage()) . " - " . $e->getMessage(), null, $logFile);
        //echo convert(memory_get_usage()) . " - " . $e->getMessage() . "\n";

    }

    Mage::log("IMPORT COMPLETE.", null, $logFile);
    echo "IMPORT COMPLETE.\n";

?>